package com.mihula.newsscraper.tests;

import com.mihula.newsscraper.entity.Article;
import com.mihula.newsscraper.repository.ArticleRepository;
import com.mihula.newsscraper.service.ArticleService;
import com.mihula.newsscraper.vo.ArticleVO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
public class ArticleServiceTest {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleRepository articleRepository;

    @Before
    public void setUp() {
        articleRepository.save(new Article("mock headline keyword1", "www.mockurl1.com"));
        articleRepository.save(new Article("keyword22 no word", "www.mockurl22.com"));
        articleRepository.save(new Article("keyword333 headline article", "www.mockurl333.com"));
        articleRepository.save(new Article("article keyword4444", "www.mockurl4444.com"));
    }

    @After
    public void clean() {
        articleRepository.deleteAll();
    }

    @Test
    public void shouldFindArticlesByKeywords() {
        List<String> keywords = new ArrayList<>(Arrays.asList("headline", "word"));
        List<ArticleVO> articles = articleService.findByKeywords(keywords);

        Assert.assertEquals(3, articles.size());
    }

    @Test
    public void shouldNotFindArticlesByKeywords() {
        List<String> keywords = new ArrayList<>(Arrays.asList("name", "animal"));
        List<ArticleVO> articles = articleService.findByKeywords(keywords);

        Assert.assertEquals(0, articles.size());
    }

    @Test
    public void shouldNotFindArticlesNoKeywords() {
        List<String> keywords = new ArrayList<>();
        List<ArticleVO> articles = articleService.findByKeywords(keywords);

        Assert.assertEquals(0, articles.size());
    }
}

