package com.mihula.newsscraper.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Article {

    @Id
    private String id;

    @TextIndexed
    private String headline;

    private String url;

    public Article(String headline, String url) {
        this.headline = headline;
        this.url = url;
    }
}
