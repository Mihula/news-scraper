package com.mihula.newsscraper.vo;

import com.mihula.newsscraper.entity.Article;
import lombok.Getter;

@Getter
public class ArticleVO {

    private String text;
    private String url;

    public ArticleVO(Article article) {
        this.text = article.getHeadline();
        this.url = article.getUrl();
    }
}
