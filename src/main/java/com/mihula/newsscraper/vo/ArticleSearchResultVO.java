package com.mihula.newsscraper.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class ArticleSearchResultVO {

    private List<ArticleVO> articles;
}
