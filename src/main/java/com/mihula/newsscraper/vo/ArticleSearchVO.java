package com.mihula.newsscraper.vo;

import lombok.Getter;

import java.util.List;

@Getter
public class ArticleSearchVO {

    private List<String> keywords;
}
