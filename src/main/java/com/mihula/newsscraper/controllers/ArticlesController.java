package com.mihula.newsscraper.controllers;

import com.mihula.newsscraper.service.ArticleService;
import com.mihula.newsscraper.vo.ArticleSearchResultVO;
import com.mihula.newsscraper.vo.ArticleSearchVO;
import com.mihula.newsscraper.vo.ArticleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticlesController {

    @Autowired
    private ArticleService articleService;

    @PostMapping("/find")
    public ArticleSearchResultVO find(@RequestBody ArticleSearchVO search) {
        List<ArticleVO> articles = articleService.findByKeywords(search.getKeywords());

        return new ArticleSearchResultVO(articles);
    }
}