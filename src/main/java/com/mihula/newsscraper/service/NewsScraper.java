package com.mihula.newsscraper.service;

import com.mihula.newsscraper.entity.Article;
import com.mihula.newsscraper.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;

// parseArticles() could be moved here and reused by the implementations but there is always a possibility
// that parsing logic will be different for different servers
public abstract class NewsScraper {

    @Autowired
    protected ArticleRepository articleRepository;

    public abstract void scrapeArticles();

    protected void filterOldArticles(List<Article> articles) {
        Iterator<Article> iterator = articles.listIterator();

        while (iterator.hasNext()) {
            Article article = iterator.next();
            if (articleRepository.existsByUrl(article.getUrl())) {
                iterator.remove();
            }
        }
    }
}
