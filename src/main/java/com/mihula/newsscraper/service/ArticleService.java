package com.mihula.newsscraper.service;

import com.mihula.newsscraper.vo.ArticleVO;

import java.util.List;

public interface ArticleService {

    List<ArticleVO> findByKeywords(List<String> keywords);
}
