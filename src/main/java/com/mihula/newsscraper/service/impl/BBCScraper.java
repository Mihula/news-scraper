package com.mihula.newsscraper.service.impl;

import com.mihula.newsscraper.entity.Article;
import com.mihula.newsscraper.service.NewsScraper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BBCScraper extends NewsScraper {

    private final Logger logger = LoggerFactory.getLogger(BBCScraper.class);

    private static final String HTTPS = "https";

    @Value("${urls.bbc}")
    private String url;

    @Override
    public void scrapeArticles() {
        try {
            Document doc = Jsoup.connect(url).get();
            Elements elements = doc.getElementsByClass("media__link");
            List<Article> articles = parseArticles(elements);
            filterOldArticles(articles);
            articleRepository.saveAll(articles);
        } catch (Exception e) {
            logger.error("Exception occurred when contacting " + url, e);
        }
    }

    private List<Article> parseArticles(Elements elements) {
        List<Article> articles = new ArrayList<>();

        for (Element e : elements) {
            Element sourceElement = e.select("a").first();
            String headline = sourceElement.text();
            String url = formatUrl(sourceElement.attr("href"));
            articles.add(new Article(headline, url));
        }

        return articles;
    }

    private String formatUrl(String url) {
        if(url.startsWith(HTTPS)) {
            return url;
        }

        return this.url + url;
    }
}
