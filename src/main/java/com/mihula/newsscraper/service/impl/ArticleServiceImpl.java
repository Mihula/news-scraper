package com.mihula.newsscraper.service.impl;

import com.mihula.newsscraper.repository.ArticleRepository;
import com.mihula.newsscraper.service.ArticleService;
import com.mihula.newsscraper.vo.ArticleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public List<ArticleVO> findByKeywords(List<String> keywords) {
        String keywordsString = String.join(" ", keywords);

        return articleRepository.findByKeyWords(keywordsString)
                .stream()
                .map(a -> new ArticleVO(a))
                .collect(Collectors.toList());
    }
}
