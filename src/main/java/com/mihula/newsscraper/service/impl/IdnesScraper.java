package com.mihula.newsscraper.service.impl;

import com.mihula.newsscraper.entity.Article;
import com.mihula.newsscraper.service.NewsScraper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IdnesScraper extends NewsScraper {

    private final Logger logger = LoggerFactory.getLogger(IdnesScraper.class);

    @Value("${urls.idnes}")
    private String url;

    @Override
    public void scrapeArticles() {
        try {
            Document doc = Jsoup.connect(url).get();
            Elements elements = doc.select(".art-link:not([rel])"); // skips blogposts and advertisement placeholder
            List<Article> articles = parseArticles(elements);
            filterOldArticles(articles);
            articleRepository.saveAll(articles);
        } catch (Exception e) {
            logger.error("Exception occurred when contacting " + url, e);
        }
    }

    private List<Article> parseArticles(Elements elements) {
        List<Article> articles = new ArrayList<>();

        for (Element e : elements) {
            Element sourceElement = e.select("a").first();
            String headline = sourceElement.text();
            String url = sourceElement.attr("href");
            articles.add(new Article(headline, url));
        }

        return articles;
    }
}
