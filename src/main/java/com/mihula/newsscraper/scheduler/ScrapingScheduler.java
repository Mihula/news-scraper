package com.mihula.newsscraper.scheduler;

import com.mihula.newsscraper.service.NewsScraper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScrapingScheduler {

    @Autowired
    private List<NewsScraper> scrapers;

    @Scheduled(cron = "${scheduler.scraper}")
    public void scrapeArticles() {
        scrapers.forEach(s -> s.scrapeArticles());
    }
}
