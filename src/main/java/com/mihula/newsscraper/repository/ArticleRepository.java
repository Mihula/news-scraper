package com.mihula.newsscraper.repository;

import com.mihula.newsscraper.entity.Article;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends MongoRepository<Article, String> {

    boolean existsByUrl(String url);

    @Query("{$text: {$search: ?0}}")
    List<Article> findByKeyWords(String keywords);
}
